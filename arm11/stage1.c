#include "3dstypes.h"
#include "utils.h"

#define MAGIC_ADDR	((volatile void*)0xEFFFFFF8)
#define MAGIC_INT   (*(volatile int*)MAGIC_ADDR)
#define FCRAM_PA    ((void *)0x20000000)

//APPCORE code
int main()
{
	//Tell ARM9 we are here!
	MAGIC_INT = 1;
	
	while (MAGIC_INT != 2) {
		InvalidateEntireInstructionCache();
	}
	//ARM11 exception vectors restored (by the ARM9)
	//And bootloader copied to FCRAM+0

	MAGIC_INT = 3;
	
	disable_MMU(FCRAM_PA);
	
	return 0;
}
