#ifndef UTILS_H
#define UTILS_H

#include "3dstypes.h"

void disable_data_cache();
void enable_data_cache();

void disable_MMU(void *jump_pa);
void enable_MMU(void *jump_va);

extern void disable_IRQ();
extern void enable_IRQ();
extern void disable_FIQ();
extern void enable_FIQ();

extern void CleanEntireDataCache();
extern void InvalidateEntireDataCache();
extern void CleanAndInvalidateEntireDataCache();
extern void InvalidateEntireInstructionCache();
extern void DataMemoryBarrier();

extern void SleepThread(s64 nanoseconds);
extern s64 GetSystemTick(void);

#endif
