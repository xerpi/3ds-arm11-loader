TARGET_ARM9	 := arm9/stage0
TARGET_ARM11 := arm11/stage1

ARM9_LD	 = arm9/stage0.ld
ARM11_LD = arm11/stage1.ld

ARM9_C_FILES  := $(foreach dir, arm9,  $(wildcard $(dir)/*.c))
ARM9_S_FILES  := $(foreach dir, arm9,  $(wildcard $(dir)/*.S))
ARM11_C_FILES := $(foreach dir, arm11, $(wildcard $(dir)/*.c))
ARM11_S_FILES := $(foreach dir, arm11, $(wildcard $(dir)/*.S))
ARM9_OBJS  := $(ARM9_C_FILES:.c=.o)	 $(ARM9_S_FILES:.S=.o)
ARM11_OBJS := $(ARM11_C_FILES:.c=.o) $(ARM11_S_FILES:.S=.o)

PREFIX	= $(DEVKITARM)/bin/arm-none-eabi
CC      = $(PREFIX)-gcc
LD      = $(PREFIX)-ld
STRIP	= $(PREFIX)-strip
OBJCOPY = $(PREFIX)-objcopy
BIN2S	= $(DEVKITARM)/bin/bin2s

COMMON_FLAGS   = -Icommon -marm -fomit-frame-pointer -mlittle-endian -fshort-wchar
ARM9_ASFLAGS   = $(COMMON_FLAGS) -mcpu=arm946e-s -march=armv5te
ARM11_ASFLAGS  = $(COMMON_FLAGS) -mcpu=mpcore	-march=armv6k

ARM9_CFLAGS	  = -Os -Wall $(ARM9_ASFLAGS)
ARM11_CFLAGS  = -Os -Wall $(ARM11_ASFLAGS)
LDFLAGS = -nostartfiles -nostdlib
KEY = 580006192800C5F0FBFB04E06A682088
IV  = 00000000000000000000000000000000

all: Launcher.dat

Launcher.dat: $(TARGET_ARM9).bin
	python tools/3dsploit.py $(TARGET_ARM9).bin Launcher_noGW.dat
	openssl enc -aes-128-cbc -K $(KEY) -iv $(IV) -in Launcher_noGW.dat -out Launcher.dat

$(TARGET_ARM9).bin: $(TARGET_ARM9).elf
	$(OBJCOPY) -S -O binary $^ $@

$(TARGET_ARM9).elf: $(ARM9_OBJS) $(TARGET_ARM11).bin.o
	$(LD) $(LDFLAGS) -T $(ARM9_LD) $^ -o $@

$(TARGET_ARM11).bin.o: $(TARGET_ARM11).bin.s
	$(CC) $(ARM9_ASFLAGS) -c $^ -o $@
$(TARGET_ARM11).bin.s: $(TARGET_ARM11).bin
	$(BIN2S) $^ > $@
	
$(TARGET_ARM11).bin: $(TARGET_ARM11).elf
	$(OBJCOPY) -S -O binary $^ $@

$(TARGET_ARM11).elf: $(ARM11_OBJS)
	$(LD) $(LDFLAGS) -T $(ARM11_LD) $^ -o $@

arm9/%.o: arm9/%.c
	$(CC) $(ARM9_CFLAGS) -c $^ -o $@
arm9/%.o: arm9/%.S
	$(CC) $(ARM9_ASFLAGS) -c $^ -o $@

arm11/%.o: arm11/%.c
	$(CC) $(ARM11_CFLAGS) -c $^ -o $@
arm11/%.o: arm11/%.S
	$(CC) $(ARM11_ASFLAGS) -c $^ -o $@

clean:
	@rm -rf $(ARM9_OBJS) $(ARM11_OBJS) $(TARGET_ARM9).elf $(TARGET_ARM11).elf $(TARGET_ARM9).bin \
		$(TARGET_ARM11).bin $(TARGET_ARM11).bin.o $(TARGET_ARM11).bin.s Launcher.dat Launcher_noGW.dat
	@echo "Cleaned!"
	
copy: Launcher.dat
	@cp Launcher_noGW.dat "/media/$(USER)/GATEWAYNAND/Launcher.dat"
	@sync
	@echo "Copied!"
