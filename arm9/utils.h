#ifndef UTILS_H
#define UTILS_H

extern int FlushProcessDataCache(uint32_t process, void *addr, uint32_t size);

extern unsigned long get_CPSR();

extern void disable_MPU();
extern void enable_MPU();

extern unsigned long cp15_get_control_reg();
extern void cp15_set_control_reg(unsigned long n);

#endif
