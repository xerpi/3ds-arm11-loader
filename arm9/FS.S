.section .text
.arm

.global FileOpen
.type FileOpen, %function
FileOpen:
	STMFD   SP!, {R4-R10,LR}
	SUB	 SP, SP, #0x30
	
	MOV R8, R1
	
	ADD	 R7, R0, #0
	LDR	 R5, =0x809797C
	LDR	 R5, [R5]
	ADD	 R5, #8
	LDR	 R1, =0x2EA0
	ADD	 R0, R5, R1
	ADD	 R1, SP, #8
	LDR	 R4, =0x8061451
	BLX	 R4
	
	MOV	 R3, #0
	STR	 R3, [SP,#0x1C]
	STR	 R3, [SP]
	STR	 R3, [SP,#4]
	ADD	 R0, SP, #0x10
	MOV	 R1, R5
	LDR	 R2, [SP,#8]
	LDR	 R3, [SP,#0xC]
	LDR	 R4, =0x8063F91
	BLX	 R4

	LDR	 R6, [SP,#0x1C]
	ADD	 R0, SP, #0x24
	MOV	 R1, #3        @ 3=char, 4=wchar
	STR	 R1, [R0]
	MOV	 R1, #0x1C
	STR	 R1, [R0,#8]
	ADD	 R1, R7, #0
	STR	 R1, [R0,#4]
	MOV	 R0, #0
	STR	 R0, [SP, #0x20]
	MOV	 R3, R8      @ open flags
	STR	 R3, [SP]
	STR	 R0, [SP,#4]
	ADD	 R1, SP, #0x20
	MOV	 R2, #0
	ADD	 R3, SP, #0x24
	LDR	 R10, =0x8084739
	MOV	 R0, R6
	BLX	 R10
	
	LDR	 R0, [SP, #0x20]
	ADD	 SP, SP, #0x30
	LDMFD   SP!, {R4-R10,PC}

.global FileRead
.type FileRead, %function
FileRead:
	STMFD   SP!, {R4-R5,LR}
	SUB	 SP, SP, #0x20
	ADD	 R4, SP, #0x10
	STR	 R2, [SP,#0x8]
	LDR	 R2, =0x80944C8
	STR	 R2, [R4]
	STR	 R1, [R4,#4]
	STR	 R4, [SP,#0x0]
	MOV	 R1, #0
	STR	 R1, [SP,#0x4]
	ADD	 R1, SP, #0xC
	MOV	 R2, R3  @ read offset
	MOV	 R3, #0
	LDR	 R5, [R0]
	LDR	 R5, [R5, #0x38]
	BLX	 R5
	LDR R0, [SP, #0xC]
	ADD	 SP, SP, #0x20
	LDMFD   SP!, {R4-R5,PC}

.global FileWrite
.type FileWrite, %function  
FileWrite:
	STMFD   SP!, {R3-R12,LR}
	SUB	 SP, SP, #0x20
	ADD	 R3, SP, #0x10
	STR	 R2, [SP,#0x8]
	LDR	 R2, =0x8094490
	STR	 R2, [R3]
	STR	 R1, [R3,#4]
	STR	 R3, [SP,#0x0]
	MOV	 R1, #0
	STR	 R1, [SP,#0x4]
	ADD	 R1, SP, #0xC
	MOV	 R2, #0
	MOV	 R3, #0
	LDR	 R4, [R0]
	LDR	 R4, [R4, #0x3C]  
	BLX	 R4	
	ADD	 SP, SP, #0x20  
	LDMFD   SP!, {R3-R12,PC}
